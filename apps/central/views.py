from django.shortcuts import render, HttpResponse
from django.views import View
from docxtpl import DocxTemplate


class TestView(View):

    def get(self, request):
        return render(request, 'central/index.html')

    def post(self, request):
        name = request.POST['name']

        document = DocxTemplate('example.docx')
        context = {'company_name': name}
        document.render(context)
        # document.save("generated_doc.docx")

        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        response['Content-Disposition'] = 'attachment; filename=download.docx'
        document.save(response)

        return response
